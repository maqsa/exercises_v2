package com.javarush.task.task01.task0140;

import java.util.Scanner;

/* 
Выводим квадрат числа
*/

public class Solution {
    public static void main(String[] args) {
        int a;
        //напишите тут ваш код
        Scanner scanner = new Scanner(System.in);
        if(scanner.hasNextInt()) { // возвращает истинну если с потока ввода можно считать целое число
            a = scanner.nextInt(); // считывает целое число с потока ввода и сохраняем в переменную
            System.out.println(a*a);
        } else {
            System.out.println("Вы ввели не целое число");
        }

    }
}