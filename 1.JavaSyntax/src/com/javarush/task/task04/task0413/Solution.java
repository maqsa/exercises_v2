package com.javarush.task.task04.task0413;

/* 
День недели
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int numberDay = Integer.parseInt(reader.readLine());
        if(numberDay == 1)
            System.out.println("понедельник");
        else if(numberDay == 2)
            System.out.println("вторник");
        else if(numberDay == 3)
            System.out.println("среда");
        else if(numberDay == 4)
            System.out.println("четверг");
        else if(numberDay == 5)
            System.out.println("пятница");
        else if(numberDay == 6)
            System.out.println("суббота");
        else if(numberDay == 7)
            System.out.println("воскресенье");
        else if(numberDay > 7 || numberDay <1)
            System.out.println("такого дня недели не существует");
    }
}