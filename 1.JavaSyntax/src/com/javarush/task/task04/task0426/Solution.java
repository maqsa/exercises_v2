package com.javarush.task.task04.task0426;

/* 
Ярлыки и числа
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int m = Integer.parseInt(reader.readLine());

        if (m<0 && m%2==0)
            System.out.println("отрицательное четное число");
        else if (m<0 && m%2!=0)
            System.out.println("отрицательное нечетное число");
        else if (m>0 && m%2==0)
            System.out.println("положительное четное число");
        else if (m>0 && m%2!=0)
            System.out.println("положительное нечетное число");
        else
            System.out.println("ноль");
    }
}
