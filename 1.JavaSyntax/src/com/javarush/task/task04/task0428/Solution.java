package com.javarush.task.task04.task0428;

/* 
Положительное число
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] arr = new int[3];
        int i = 0;

        arr[0] = Integer.valueOf(reader.readLine());
        arr[1] = Integer.valueOf(reader.readLine());
        arr[2] = Integer.valueOf(reader.readLine());

        for (int a : arr) {
            if (a >= 0) {
                i++;
            }
        }

        System.out.println(i);

    }
}
