package com.javarush.task.task04.task0429;

/* 
Положительные и отрицательные числа
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] arr = new int[3];
        int countPos = 0;
        int countNeg = 0;

        arr[0] = Integer.valueOf(reader.readLine());
        arr[1] = Integer.valueOf(reader.readLine());
        arr[2] = Integer.valueOf(reader.readLine());

        for (int a : arr) {
            if (a > 0) {
                countPos++;
            } else if (a < 0) countNeg++;
        }
        System.out.println("количество отрицательных чисел: " + countNeg);
        System.out.println("количество положительных чисел: " + countPos);
    }
}
