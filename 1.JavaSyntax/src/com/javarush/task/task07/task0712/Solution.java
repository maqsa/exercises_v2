package com.javarush.task.task07.task0712;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Самые-самые
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> strings = new ArrayList<>();

        for (int i = 0; i < 10; i++)
            strings.add(reader.readLine());

        int maxL = 0;
        int minL = Integer.MAX_VALUE;
        for (String string : strings){
            if (string.length() > maxL)
                maxL = string.length();

            if (string.length() < minL)
                minL = string.length();
        }

        for (String string : strings){
            if (string.length() == minL || string.length() == maxL){
                System.out.println(string);
                break;
            }
        }
    }
}
