package com.javarush.task.task30.task3009;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* 
Палиндром?
*/

public class Solution {
    public static void main(String[] args) {
        //System.out.println(getRadix("112"));        //expected output: [3, 27, 13, 15]
       // System.out.println(getRadix("123"));        //expected output: [6]
       // System.out.println(getRadix("5321"));       //expected output: []
        System.out.println(getRadix("100000"));         //expected output: []
    }

    private static Set<Integer> getRadix(String number) {
        Set<Integer> arr = new HashSet<>();

        try {
                for (int i = 2; i <= 36; i ++) {
                    String systemNumber = Integer.toString(Integer.parseInt(number), i);
                    StringBuilder systemNumberReverse = new StringBuilder(systemNumber).reverse();
                    if (systemNumber.compareTo(String.valueOf(systemNumberReverse)) == 0) {
                        arr.add(i);
                    }
                    System.out.println(systemNumber);
                }
            }

         catch (NumberFormatException e){return arr;}

        return arr;
    }


}