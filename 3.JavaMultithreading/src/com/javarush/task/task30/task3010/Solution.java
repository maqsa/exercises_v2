package com.javarush.task.task30.task3010;

/*
Минимальное допустимое основание системы счисления
*/

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        try {
            ArrayList<Integer> arr = new ArrayList<>();
            String s = args[0];
            char[] c = s.toCharArray();

            for (char letter : c)
            {
                if(!Character.isLetterOrDigit(letter)) {
                    System.out.println("incorrect");
                    return;
                }
            }

            for (int i = 2; i <= 36; i++) {
                try {
                    BigDecimal strNumber = new BigDecimal(new BigInteger(s, i));
                    arr.add(i);

                } catch (Exception e){
                }
            }
            System.out.println(arr.get(0));


        } catch (Exception e){
            System.out.println("incorrect");
        }
    }
}