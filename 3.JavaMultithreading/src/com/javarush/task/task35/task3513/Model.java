package com.javarush.task.task35.task3513;

import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Stack;

/**
 * Created by maqsa on 10.06.2017.
 */
public class Model {
    private static final int FIELD_WIDTH = 4;
    private Stack<Tile[][]> previousStates=new Stack<>();
    private Stack previousScores = new Stack();
    private boolean isSaveNeeded = true;
    private Tile[][] gameTiles= new Tile[FIELD_WIDTH][FIELD_WIDTH];
    int score;
    int maxTile;

    private  boolean hasBoardChanged() {
        int sum1 = 0;
        int sum2 = 0;
        if(!previousStates.isEmpty()) {
            Tile[][] prevGameTiles = previousStates.peek();
            for (int i = 0; i < FIELD_WIDTH; i++) {
                for (int j = 0; j < FIELD_WIDTH; j++) {
                    sum1 += gameTiles[i][j].value;
                    sum2 += prevGameTiles[i][j].value;
                }
            }
        }
        return sum1 != sum2;
    }

    public void autoMove() {
        PriorityQueue<MoveEfficiency> moves = new PriorityQueue<>(4, Collections.reverseOrder());
        moves.offer(getMoveEfficiency(new Move() {
            @Override
            public void move() {
                left();
            }
        }));
        moves.offer(getMoveEfficiency(new Move() {
            @Override
            public void move() {
                right();
            }
        }));
        moves.offer(getMoveEfficiency(new Move() {
            @Override
            public void move() {
                up();
            }
        }));
        moves.offer(getMoveEfficiency(new Move() {
            @Override
            public void move() {
                down();
            }
        }));
        moves.peek().getMove().move();
    }

    public MoveEfficiency getMoveEfficiency(Move move){
        MoveEfficiency moveEfficiency;
        move.move();
        if(hasBoardChanged()){
            moveEfficiency = new MoveEfficiency(getEmptyTiles().size(), score, move);
        }else{
            moveEfficiency = new MoveEfficiency(-1, 0, move);
        }
        rollback();
        return moveEfficiency;
    }

    public void randomMove(){
        int n = ((int) (Math.random() * 100)) % 4;
        switch (n){
            case 0 : down();    break;
            case 1 : right();   break;
            case 2 : up();      break;
            case 3 : left();    break;
        }
    }
    private void saveState(Tile[][] tiles){
        Tile[][] bufTiles=new Tile[FIELD_WIDTH][FIELD_WIDTH];
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                bufTiles[i][j]= new Tile();
                bufTiles[i][j].value= tiles[i][j].value;
            }
        }
        previousStates.push(bufTiles);
        previousScores.push(this.score);
        isSaveNeeded=false;
    }

    public void rollback(){
        if(!previousScores.isEmpty()&&!previousStates.isEmpty()) {
            gameTiles =  previousStates.pop();
            score= (int) previousScores.pop();

        }
    }

    public boolean canMove() {
        if(!getEmptyTiles().isEmpty())
            return true;
        for(int i = 0; i < gameTiles.length; i++) {
            for(int j = 1; j < gameTiles.length; j++) {
                if(gameTiles[i][j].value == gameTiles[i][j-1].value)
                    return true;
            }
        }
        for(int j = 0; j < gameTiles.length; j++) {
            for(int i = 1; i < gameTiles.length; i++) {
                if(gameTiles[i][j].value == gameTiles[i-1][j].value)
                    return true;
            }
        }
        return false;
    }

    public Model(){
        resetGameTiles();
    }

    public Tile[][] getGameTiles() {
        return gameTiles;
    }
    private void rotateToRight() {
        Tile[][] rotate = new Tile[FIELD_WIDTH][FIELD_WIDTH];
        for (int i =0; i<FIELD_WIDTH; i++) {
            for (int j = 0; j < FIELD_WIDTH; j++) {
                rotate[i][j] = gameTiles[FIELD_WIDTH - j - 1][i];
            }
        }
        gameTiles = rotate;
    }

    void right() {
        saveState(gameTiles);
        rotateToRight();
        rotateToRight();
        left();
        rotateToRight();
        rotateToRight();
    }

    void up() {
        saveState(gameTiles);
        rotateToRight();
        rotateToRight();
        rotateToRight();
        left();
        rotateToRight();
    }

    void down() {
        saveState(gameTiles);
        rotateToRight();
        left();
        rotateToRight();
        rotateToRight();
        rotateToRight();
    }

    void left() {
        if(isSaveNeeded) saveState(gameTiles);

        int j = 0;
        for (int i = 0; i < gameTiles.length; i++) {
            if (compressTiles(gameTiles[i]) | mergeTiles(gameTiles[i]))
                j++;
        }
        isSaveNeeded = true;

        if (j != 0) {
            addTile();
        }
    }

    private boolean mergeTiles(Tile[] tiles) {
        boolean change = false;
        for (int i = 1; i < tiles.length; i++) {
            if ((tiles[i - 1].value == tiles[i].value) && !tiles[i - 1].isEmpty() && !tiles[i].isEmpty()) {
                change = true;
                tiles[i - 1].value *= 2;
                score += tiles[i - 1].value;
                maxTile = maxTile > tiles[i - 1].value ? maxTile : tiles[i - 1].value;
                tiles[i] = new Tile();
                compressTiles(tiles);
            }
        }
        return change;
    }

    private boolean compressTiles(Tile[] tiles) {
        boolean change = false;
        for (int i = 1; i < tiles.length; i++) {
            for (int j = 1; j < tiles.length; j++) {
                if (tiles[j - 1].isEmpty() && !tiles[j].isEmpty()) {
                    change = true;
                    tiles[j - 1] = tiles[j];
                    tiles[j] = new Tile();
                }
            }
        }
        return change;
    }

    private void addTile(){
        ArrayList<Tile> emptyTiles = getEmptyTiles();
        if (!emptyTiles.isEmpty()) {
            int num = (int) (Math.random() * emptyTiles.size());
            emptyTiles.get(num).value = Math.random() < 0.9 ? 2 : 4;
        }
    }

    private ArrayList<Tile> getEmptyTiles(){
        ArrayList<Tile> emptyTiles = new ArrayList<>();
        for(int i = 0; i < FIELD_WIDTH; i++){
            for(int j = 0; j < FIELD_WIDTH; j++){
                if(gameTiles[i][j].isEmpty())
                    emptyTiles.add(gameTiles[i][j]);
            }
        }
        return emptyTiles;
    }

    public void resetGameTiles(){
        for(int i = 0; i < FIELD_WIDTH; i++){
            for(int j = 0; j < FIELD_WIDTH; j++){
                gameTiles[i][j] = new Tile();
            }
        }

        score = 0;
        maxTile = 2;

        addTile();
        addTile();
    }

}
