package com.javarush.task.task31.task3111;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.FileVisitResult.CONTINUE;

public class SearchFileVisitor extends SimpleFileVisitor<Path> {
    private String  partOfName;
    private String  partOfContent;
    private int     minSize;
    private int     maxSize;
    private boolean flagPartOfName;
    private boolean flagPartOfContent;
    private boolean flagMinSize;
    private boolean flagMaxSize;
    private List<Path> foundFiles = new ArrayList<>();

    public SearchFileVisitor() {
        this.partOfName         =   null;
        this.partOfContent      =   null;
        this.minSize            =   0;
        this.maxSize            =   0;
        this.flagMaxSize        =   false;
        this.flagMinSize        =   false;
        this.flagPartOfContent  =   false;
        this.flagPartOfName     =   false;

    }

    public void setPartOfName(String partOfName) {
        this.partOfName         =   partOfName;
        this.flagPartOfName     =   true;
    }

    public void setPartOfContent(String partOfContent) {
        this.partOfContent      =   partOfContent;
        this.flagPartOfContent  =   true;
    }

    public void setMinSize(int minSize) {
        this.minSize            =   minSize;
        this.flagMinSize        =   true;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize            =   maxSize;
        this.flagMaxSize        =   true;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (!attrs.isRegularFile()) return CONTINUE;

        if (flagPartOfName && file.getFileName().toString().indexOf(this.partOfName) == -1)
            return CONTINUE;

        if (flagMinSize && attrs.size() < minSize)
            return CONTINUE;

        if (flagMaxSize && attrs.size() > maxSize)
            return CONTINUE;

        if (flagPartOfContent && new String(Files.readAllBytes(file)).indexOf(partOfContent) == -1)
            return CONTINUE;

        foundFiles.add(file);

        return CONTINUE;
    }

    public List<Path> getFoundFiles() {
        return foundFiles;
    }
}
